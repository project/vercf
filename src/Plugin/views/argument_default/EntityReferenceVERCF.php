<?php

namespace Drupal\vercf\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\node\NodeInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Default argument plugin to use the raw value from the URL.
 *
 * @ingroup views_argument_default_plugins
 *
 * @ViewsArgumentDefault(
 *   id = "entityreferencevercf",
 *   title = @Translation("VERCF Entity Reference")
 * )
 */
class EntityReferenceVERCF extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  /**
   * Token Handler.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The OpenID Connect logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a Raw object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempStore Service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route_match Service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger Service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PrivateTempStoreFactory $temp_store_factory, RouteMatchInterface $route_match, LoggerChannelFactoryInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->tempStoreFactory = $temp_store_factory;
    $this->routeMatch = $route_match;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('tempstore.private'),
      $container->get('current_route_match'),
      $container->get('logger.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['vercf_field_name'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['vercf_field_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('field machine name'),
      '#default_value' => $this->options['vercf_field_name'],
      '#description' => $this->t('Machine name field to use as fielter.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    $node = $this->routeMatch->getParameter('node');
    if ($node instanceof NodeInterface) {
      $field_name = $this->options['vercf_field_name'];

      if (!$node->hasField($field_name)) {
        $this->logger->get('vercf')->warning(
          'Field @field_name does not exist, check the views where you use vercf filter',
          ['@field_name' => $field_name]
        );
        return NULL;
      }

      if ($node->get($field_name)->getValue()) {
        $target_id = $node->$field_name->getValue();
        $target_id_count = count($target_id);
        if ($target_id_count = 1) {
          return $target_id[0]['target_id'];
        }
        elseif ($target_id_count > 1) {
          return implode(',', $target_id);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['url'];
  }

}
